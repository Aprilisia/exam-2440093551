import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './form/form.component';
import { IndexComponent } from './index/index.component';

const routes: Routes = [
  { path: "user", redirectTo: "user/index", pathMatch: "full" },
  { path: "user/index", component: IndexComponent },
  { path: "user/create", component: FormComponent },
  { path: "user/edit/:idUser", component: FormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
