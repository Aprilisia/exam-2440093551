import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { FormComponent } from './form/form.component';

const routes: Routes = [
  { path: "pembelian", redirectTo: "pembelian/index", pathMatch: "full" },
  { path: "pembelian/index", component: IndexComponent },
  { path: "pembelian/create", component: FormComponent },
  { path: "pembelian/edit/:idPembelian", component: FormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PembelianRoutingModule { }
