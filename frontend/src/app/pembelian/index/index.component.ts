import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent {
  username = localStorage.getItem("username");
  role = localStorage.getItem("role");
  isLoggedIn = localStorage.getItem("username") && localStorage.getItem("role") ? true : false;

  pembelian: any;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    if(this.isLoggedIn && this.role != "Admin") {
      this.http.get("http://127.0.0.1:8000/api/pembelian/")
      .subscribe(data => {
        this.pembelian = data;
      })
    } else {
      if(this.role == "Admin")
        this.router.navigateByUrl('/')
      else
        this.router.navigateByUrl('/login')
    }
  }

  deletePembelian(id: number){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': "application/json"
      })
    };

    this.http.delete("http://127.0.0.1:8000/api/pembelian/" + id, httpOptions)
    .subscribe(data => {
      this.pembelian = this.pembelian.filter((item: { id: number; }) => item.id !== id);
      alert("Success")
    })
  }

  validate(id: number){
    fetch(
      `http://127.0.0.1:8000/api/pembelian/validasi/${id}`,
      {
        method: "PUT",
        headers: {
          'Accept': 'application/json'
        },
      }
    )
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      if(data.status === 200) {
        alert("Success");
        this.router.navigateByUrl("/pembelian")
        window.location.href = "/pembelian"
      } else
        alert("Failed");
    })
    .catch((err) => {
      alert(err.message);
      console.log(err);
    });
  }
}
