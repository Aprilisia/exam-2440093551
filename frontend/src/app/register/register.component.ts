import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  form: FormGroup | any;
  contents: any;

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {    
    this.form = this.formBuilder.group({
      nama:  new FormControl("", [ Validators.required ]),
      username:  new FormControl("", [ Validators.required ]),
      password:  new FormControl("", [ Validators.required ]),
    });
  }

  get f(){
    return this.form.controls;
  }

  submit(){
    var formData = new FormData();
    formData.append('nama', this.form.value.nama);
    formData.append('role', "Perakit");
    formData.append('username', this.form.value.username);
    formData.append('password', this.form.value.password);
    console.log(formData)
    fetch(
      `http://127.0.0.1:8000/api/user`,
      {
        method: "POST",
        headers: {
          'Accept': 'application/json'
        },
        body: formData
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        alert(data.message);
        if(data.status === 200) {
          let response = data.data
          localStorage.setItem('id_user', response.id)
          localStorage.setItem('role', response.role)
          localStorage.setItem('username', response.username)
          window.location.href = "/login";
        }
      })
      .catch((err) => {
        alert(err.message);
        console.log(err);
      });
    }
}