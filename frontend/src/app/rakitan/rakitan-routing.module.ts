import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { FormComponent } from './form/form.component';

const routes: Routes = [
  { path: "rakitan", redirectTo: "rakitan/index", pathMatch: "full" },
  { path: "rakitan/index", component: IndexComponent },
  { path: "rakitan/create", component: FormComponent },
  { path: "rakitan/edit/:idRakitan", component: FormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RakitanRoutingModule { }
