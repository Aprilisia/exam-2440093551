import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  username = localStorage.getItem("username");
  role = localStorage.getItem("role");
  isLoggedIn = localStorage.getItem("username") && localStorage.getItem("role") ? true : false;
  rakitan: any;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    if(this.isLoggedIn) {
      this.http.get("http://127.0.0.1:8000/api/rakitan/")
      .subscribe(data => {
        this.rakitan = data;
      })
    } else {
      if(this.role == "Perakit")
        this.router.navigateByUrl('/')
      else
        this.router.navigateByUrl('/login')
    }
  }

  deleteRakitan(id: number){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': "application/json"
      })
    };

    this.http.delete("http://127.0.0.1:8000/api/rakitan/" + id, httpOptions)
    .subscribe(data => {
      this.rakitan = this.rakitan.filter((item: { id: number; }) => item.id !== id);
      alert("Success")
    })
  }
}
