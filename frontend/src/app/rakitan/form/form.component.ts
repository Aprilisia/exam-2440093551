import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class FormComponent implements OnInit {
  id_user = localStorage.getItem("id_user");
  username = localStorage.getItem("username");
  role = localStorage.getItem("role");
  isLoggedIn = localStorage.getItem("username") && localStorage.getItem("role") ? true : false;

  id: number | any;
  rakitan: any;
  form: FormGroup | any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    if(this.isLoggedIn) {
      this.id = this.route.snapshot.params['idRakitan'] ?? "";
      
      this.form = this.formBuilder.group({
        nama:  new FormControl("", [ Validators.required ]),
        solusi:  new FormControl("", [ Validators.required ]),
        created_by:  new FormControl(this.id_user, [ Validators.required ]),
      });
  
      if(this.id) {
        this.http.get("http://127.0.0.1:8000/api/rakitan/" + this.id)
        .subscribe(x => this.form.patchValue(x))
      }
    } else {
      if(this.role == "Perakit")
        this.router.navigateByUrl('/')
      else
        this.router.navigateByUrl('/login')
    }
  }

  get f(){
    return this.form.controls;
  }

  submit(){
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    var formData = new FormData();
    formData.append('nama', this.form.value.nama);
    formData.append('solusi', this.form.value.solusi);
    formData.append('created_by', this.form.value.created_by);
    
    this.http.post("http://127.0.0.1:8000/api/rakitan/" + this.id ?? "", formData, httpOptions)
      .subscribe(data => {
        alert("Success")
        this.router.navigateByUrl('rakitan/index');
      }); 
  }
}
