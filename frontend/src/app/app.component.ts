import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Tugas Kelompok 4';
  username = localStorage.getItem("username");
  role = localStorage.getItem("role");
  isLoggedIn = localStorage.getItem("username") && localStorage.getItem("role") ? true : false;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    console.log(  )
    if(!this.isLoggedIn && !(window.location.pathname == "/register")) 
      this.router.navigateByUrl("/login")
  }

  logout() {
    localStorage.clear()
    window.location.href = "/login";
  }
}
