<?php

namespace App\Http\Controllers;

use App\Models\Rakitan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use function PHPUnit\Framework\isNull;

class RakitanController extends Controller
{
    public function get() 
    {
        $users = Rakitan::all();
        return response()->json($users, 200);
    }

    public function getById($id){
        $data = Rakitan::find($id);
        return response()->json($data, 200);
    }

    public function save(Request $request, $id = null) 
    {
        $dataExist = Rakitan::find($id);
        
        $data['nama'] = $request['nama'];
        $data['solusi'] = $request['solusi'];
        $data['created_by'] = $request['created_by'];
        
        $res = false;
        if(!empty($id)) {
            $res = $dataExist->update($data);
        } else {
            $res = Rakitan::create($data);
        }

        if($res) {
            return response()->json([
                'message' => "Success",
                'success' => true
            ], 200);
        } else {
            return response()->json([
                'message' => "Failed",
                'success' => true
            ], 500);
        }
    }

    public function delete($id){
        $data = Rakitan::find($id);
        $isFileDelete = Storage::disk('public')->delete($data->gambar);
        if($isFileDelete) {
            $res = Rakitan::find($id)->delete();
            if($res) {
                return response()->json([
                    'message' => "Successfully deleted",
                    'success' => true
                ], 200);
            }
        }
        return response()->json([
            'message' => "Failed deleted",
            'success' => true
        ], 500);
    }
}
